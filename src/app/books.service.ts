import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class BooksService {
 
  /*
  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ultrices venenatis quam id commodo. Integer rutrum turpis sapien, non gravida ex porttitor in. Fusce vitae ex ac arcu ultricies pellentesque id in metus. Morbi pellentesque aliquam rutrum. Sed eget mauris quis risus mollis porta. Nunc quis enim lacinia, lobortis risus ac, egestas urna. Morbi fringilla lorem sed ligula posuere finibus id sed ipsum. Praesent dapibus massa sem, id dignissim orci varius ut. Sed ac nisi imperdiet, lobortis magna vel, tristique ligula."},
           {title:'War and Peace', author:'Leo Tolstoy', summary:"Aenean quis egestas ante. Praesent sagittis tincidunt nisi, ut tincidunt dolor varius ac. Vivamus eu nulla eleifend, cursus arcu id, sagittis nisi. Ut tincidunt ultricies mauris, sed viverra ante viverra ac. Sed enim sem, posuere a elementum a, gravida at urna. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam ornare ante in felis fringilla, quis tincidunt lorem commodo. In congue felis et magna feugiat facilisis. Pellentesque vulputate turpis nisl, in aliquam diam consequat eu. Donec sed ultrices magna. Aenean eu iaculis dui. Cras non sapien sit amet ex finibus mattis. Aliquam pharetra ipsum non massa bibendum, eget vehicula metus efficitur. Maecenas consectetur urna risus, sit amet commodo odio rutrum a. Aliquam sit amet leo at tortor facilisis efficitur. Integer hendrerit neque id laoreet lobortis."},
           {title:'The Magic Mountain', author:'Thomas Mann', summary:"Nullam nec est ornare, hendrerit quam euismod, luctus velit. Pellentesque tristique non erat vehicula bibendum. Quisque lacinia diam sed varius molestie. In hac habitasse platea dictumst. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec aliquam semper interdum. Etiam dictum tempus pretium. Morbi pretium eleifend dignissim. Nulla malesuada augue leo, ut placerat mauris rhoncus non."}];


  public addBooks() {
    setInterval(()=>this.books.push({title:'A new one', author:'New author', summary:'Short summary'}),2000);
  }

*/
  
  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  public getBooks(userId, startAfter){
    this.bookCollection = this.db.collection(`users/${userId}/books`,
    ref => ref.orderBy('title', 'asc').limit(5).startAfter(startAfter));
    return this.bookCollection.snapshotChanges()
  }

/*
  public getBooks(userId){
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    return this.bookCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
  }
*/

  public deleteBook(userId:string , id:string){
    this.db.doc(`users/${userId}/books/${id}`).delete();
  }

  public updateBook(userId:string, id:string, title:string, author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
        title:title,
        author:author
      }
    )
  }

  addBook(userId:string, title:string, author:string){
    const book = {title:title, author:author};
    this.userCollection.doc(userId).collection('books').add(book)
  }

  /*
  public getBooks(){
    const booksObservable = new Observable(observer => {
      setInterval(()=>observer.next(this.books),500)
    });
    return booksObservable;
  }
  /*
 /*
  public getBooks(){
    return this.books;
  }
  */

  /*
  
  
  getBooks(userId):Observable<any[]>{

  }
  */

  constructor(private db:AngularFirestore) { }


}
