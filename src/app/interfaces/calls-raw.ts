export interface CallsRaw {

    [index: number]: {
    userId:number,
    id:number,
    title: string,
    body: string
    }

}
