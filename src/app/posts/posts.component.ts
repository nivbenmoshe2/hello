import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from '../blog.service';

@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  blogs$;
  panelOpenState = false;
  hasError:boolean = false;
  errorMessage:string;

  constructor(private route:ActivatedRoute, private blogService:BlogService) { }

  ngOnInit(): void {
    this.blogs$ = this.blogService.getblog();
  }

}
