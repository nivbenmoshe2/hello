import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  public images:string[] =[];
  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-nivbn.appspot.com/o/';

  constructor() {
    this.images[0] = this.path + 'biz.JPG' + '?alt=media&token=0931c07f-74b6-4465-8c51-f3f834575be7';
    this.images[1] = this.path + 'entermnt.JPG' + '?alt=media&token=064f5183-8d25-416f-902a-680fe40d2a4e';
    this.images[2] = this.path + 'politics-icon.png' + '?alt=media&token=0da8efbb-0c69-4dbc-9ada-1c81914fdca8';
    this.images[3] = this.path + 'sport.JPG' + '?alt=media&token=e4ff98d5-b7b2-47fa-b3b6-ea75ab241c86';
    this.images[4] = this.path + 'tech.JPG' + '?alt=media&token=981bd2b3-d719-4e88-bcff-7c042e72e188';
   }
}
