import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CallsRaw } from './interfaces/calls-raw';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  private URL = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private http:HttpClient) { }

  getblog(){
    return this.http.get<CallsRaw>(`${this.URL}`);
  }


}
