// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBnuwGqzxTxc847RtZxtgRcBJHQ1DIaqLs",
    authDomain: "hello-nivbn.firebaseapp.com",
    databaseURL: "https://hello-nivbn.firebaseio.com",
    projectId: "hello-nivbn",
    storageBucket: "hello-nivbn.appspot.com",
    messagingSenderId: "650768472769",
    appId: "1:650768472769:web:76dd9591c32390b4705240"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
